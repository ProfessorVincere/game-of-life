using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using UnityAtoms.BaseAtoms;

public class Interactor : MonoBehaviour
{
    [SerializeField] private SOCamera mainCamera;
    [SerializeField] private BoolVariable isTurning;
    [SerializeField] private LayerMask interactableLayers;
    private Ray ray;
    private RaycastHit hitObject;

    [SerializeField, BoxGroup("Events")] private InteractableEvent onInteractWithInteractable;

    public void ClickAndInteract()
    {
        if (Helper.isOverUI() || isTurning.Value) return;

        ray = mainCamera.Value.ScreenPointToRay(Mouse.current.position.ReadValue());
        if (Physics.Raycast(ray, out hitObject, Mathf.Infinity, interactableLayers))
        {
            if (hitObject.collider.gameObject.TryGetComponent(out Interactable interactableComponent))
            {
                interactableComponent.Interact(this);
                onInteractWithInteractable.Invoke(interactableComponent);
            }
        }
    }
}

[System.Serializable]
public class InteractableEvent : UnityEvent<Interactable> { }
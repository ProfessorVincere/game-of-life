using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    [SerializeField] private bool isInteracting = true;
    private List<IInteractable> interactables;

    [SerializeField] private UnityEvent OnInteract;

    private void Awake()
    {
        interactables = GetComponents<IInteractable>().ToList();
    }

    public void Interact(Interactor interactor)
    {
        if (!isInteracting) return;

        foreach (IInteractable currentInteractable in interactables)
        {
            currentInteractable.React(interactor);
        }

        OnInteract.Invoke();
    }
}

public interface IInteractable
{
    void React(Interactor interactor);
}
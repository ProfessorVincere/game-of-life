using UnityEngine;
using TMPro;

public class SwitchTextDisplay : MonoBehaviour
{
    [SerializeField] TMP_Text textObject;
    [SerializeField] private string positiveValueTextOutput;
    [SerializeField] private string negativeValueTextOutput;

    public void SetSwitchTextValue(bool value) => textObject.text = value ? positiveValueTextOutput : negativeValueTextOutput;
}

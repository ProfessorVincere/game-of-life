using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuffixTextDisplay : BasicTextDisplay
{
    public string suffix;

    public override void UpdateText(SOInteger intVariable)
    {
        textDisplay.text = intVariable.Value.ToString() + suffix;
    }
}

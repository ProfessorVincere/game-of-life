using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;
using TMPro;


public class BasicTextDisplay : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI textDisplay;

    private void Reset()
    {
        textDisplay = GetComponent<TextMeshProUGUI>();
    }
    public virtual void UpdateText(string textValue)
    {
        textDisplay.text = textValue;
    }

    public virtual void UpdateText(SOFloat floatVariable)
    {
        textDisplay.text = floatVariable.Value.ToString();
    }

    public virtual void UpdateText(SOString stringVariable)
    {
        textDisplay.text = stringVariable.Value;
    }

    public virtual void UpdateText(SOInteger intVariable)
    {
        textDisplay.text = intVariable.Value.ToString();
    }
    public virtual void UpdateText(float value)
    {
        textDisplay.text = value.ToString("F1");
    }
    public virtual void UpdateText(int value)
    {
        textDisplay.text = value.ToString();
    }
}

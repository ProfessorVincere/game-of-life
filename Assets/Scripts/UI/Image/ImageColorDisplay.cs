using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.UI;

public class ImageColorDisplay : MonoBehaviour
{
    [SerializeField] private Image targetImage;
    [SerializeField] private ColorVariable presetColor;

    private void Reset()
    {
        targetImage = GetComponent<Image>();
    }

    private void Awake()
    {
        targetImage.color = presetColor.Value;
    }
}

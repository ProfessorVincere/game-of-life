using UnityEngine;
using UnityEngine.UI;

public class ImageFillDisplay : MonoBehaviour
{
    [SerializeField] private float maxValue;
    [SerializeField] private Image image;

    public void SetFillValue(SOFloat value)
    {
        image.fillAmount = Calculation.NormalizedValue(value.Value, 0, maxValue);
    }
}

using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;

public class SliderDisplay : MonoBehaviour
{
    [SerializeField] Slider sliderObject;
    [SerializeField] FloatVariable minimumValue, maximumValue;

    private void Awake()
    {
        sliderObject.minValue = minimumValue.Value;
        sliderObject.maxValue = maximumValue.Value;
        sliderObject.value = minimumValue.Value;
    }

    public void SetSliderValue(SOFloat floatVariable)
    {
        sliderObject.value = floatVariable.Value;
    }
}

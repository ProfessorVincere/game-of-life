using UnityEngine;

public class BillboardCanvas : MonoBehaviour
{
    [SerializeField] SOCamera uiCamera;

    Quaternion originalRotation;

    void Start()
    {
        originalRotation = transform.rotation;
    }

    void Update()
    {
        transform.rotation = uiCamera.Value.transform.rotation * originalRotation;
    }
}

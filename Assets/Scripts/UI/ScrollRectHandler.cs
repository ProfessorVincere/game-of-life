using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class ScrollRectHandler : MonoBehaviour
{
    [SerializeField] ScrollRect rectObject;

    public void ResetToTop()
    {
        rectObject.normalizedPosition = new Vector2(0, 1);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonActor : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler
{
	const float LONG_CLICK = 0.25f;

	public UnityEvent OnClick;
	public UnityEvent OnPress;
	public UnityEvent OnLongPress;
	public UnityEvent OnPressRelease;
	public UnityEvent OnHoverEnter;
	public UnityEvent OnHoverExit;

	bool isPointerDown;
	float curTime;

	public void OnPointerDown(PointerEventData eventData)
	{
		isPointerDown = true;
		OnClick.Invoke();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		isPointerDown = false;
		OnPressRelease.Invoke();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		OnHoverEnter.Invoke();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		OnHoverExit.Invoke();
	}

	void Update()
	{
		if (isPointerDown)
		{
			curTime += Time.deltaTime;

			if(curTime > LONG_CLICK)
			{
				OnLongPress.Invoke();
				isPointerDown = false;
				curTime = 0;
			}
		}
		else
		{
			if(curTime > 0)
			{
				OnPress.Invoke();
			}
			
			curTime = 0;
		}
	}
}

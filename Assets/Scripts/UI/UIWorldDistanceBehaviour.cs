using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWorldDistanceBehaviour : MonoBehaviour
{
	//This is probably going to be removed as the player will be locked on place when moving
	[SerializeField] SOTransform target;
	[SerializeField] Transform refObject;
	[SerializeField] Canvas canvas;
	[SerializeField] float maxDistance;

	void Update()
	{
		if (canvas.enabled)
		{
			if(maxDistance < Vector3.Distance(target.Value.position, refObject.position))
			{
				canvas.enabled = false;
			}
		}
	}
}

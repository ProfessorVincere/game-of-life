using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

public class UIElementPositionSwitch : MonoBehaviour
{
    [SerializeField] private RectTransform targetElement;
    [SerializeField] private float switchSpeed;
    [SerializeField, ReadOnly] private Vector2 targetPosition;
    [SerializeField, ReadOnly] private Vector2 initialPosition;

    private bool isInitial = true;

    [Button]
    public void SetTargetPosition()
    {
        targetPosition = targetElement.anchoredPosition;
    }

    [Button]
    public void SetInitialPosition()
    {
        initialPosition = targetElement.anchoredPosition;
    }

    [Button]
    public void SetToInitialPosition()
    {
        targetElement.anchoredPosition = initialPosition;
    }

    public void SwitchPosition()
    {
        targetElement.DOKill();

        Vector2 setPosition = isInitial ? targetPosition : initialPosition;

        targetElement.DOAnchorPos(setPosition, 1 / switchSpeed).SetEase(Ease.InBounce);

        isInitial = !isInitial;
    }

}

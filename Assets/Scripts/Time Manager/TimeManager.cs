using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class TimeManager : Singleton<TimeManager>
{
    [SerializeField] private FloatConstant minimumSpeed, maximumSpeed;
    [SerializeField] private FloatVariable currentSpeed;

    public void SetTimeSpeed(float value)
    {
        float newTimeScale = Mathf.Clamp(value, minimumSpeed.Value, maximumSpeed.Value);
        currentSpeed.Value = newTimeScale;
        Time.timeScale = newTimeScale;
    }
}

using UnityEngine;
using UnityAtoms.BaseAtoms;

[RequireComponent(typeof(Cell))]
public class CellStatusVisualizer : CellComponent
{
    [SerializeField] private SpriteRenderer cellSpriteRenderer;
    [SerializeField] private ColorVariable aliveStatusColor;
    [SerializeField] private ColorVariable deadStatusColor;

    //called on cell event
    public void SetCellColor(bool statusValue)
    {
        cellSpriteRenderer.color = statusValue ? aliveStatusColor.Value : deadStatusColor.Value;
    }

    public void SetCellColor(Color colorValue)
    {
        if (entity.IsAlive)
            cellSpriteRenderer.color = colorValue;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellGenerationBehavior : CellComponent
{
    [SerializeField] private CellGrid grid;

    private bool nextAliveState = false;

    public void CalculateReaction()
    {

        if (entity.IsAlive)
        {
            if (TotalLiveNeighbors() < 2 || TotalLiveNeighbors() > 3)
            {
                nextAliveState = false;
            }
            else
            {
                nextAliveState = true;
            }
        }
        else
        {
            if (TotalLiveNeighbors() == 3)
            {
                nextAliveState = true;
            }
            else
            {
                nextAliveState = false;
            }
        }
    }

    public void ReactToCells()
    {
        entity.SetStatusAlive(nextAliveState);
    }

    int TotalLiveNeighbors()
    {
        int liveCount = 0;

        int startRow = (int)entity.GridIndex.x;
        int startColumn = (int)entity.GridIndex.y;

        for (int searchRow = startRow - 1; searchRow <= startRow + 1; searchRow++)
        {
            for (int searchCol = startColumn - 1; searchCol <= startColumn + 1; searchCol++)
            {
                if (grid.cellDictionary.TryGetValue(new Vector2(searchRow, searchCol), out Cell value))
                {
                    if (value.IsAlive && (value != entity))
                    {
                        liveCount++;
                    }
                }
            }
        }

        return liveCount;
    }
}

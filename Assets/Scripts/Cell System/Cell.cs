using UnityEngine;
using UnityEngine.Events;
using UnityAtoms.BaseAtoms;
using Sirenix.OdinInspector;

public class Cell : MonoBehaviour, IInteractable
{
    private bool isAlive;
    private Vector2 gridIndex;

    [SerializeField, BoxGroup("Events")] private VoidEvent OnInteractWithCell; 
    [SerializeField, BoxGroup("Events")] private UnityEvent<bool> OnCellToggleStatus;

    public bool IsAlive { get => isAlive; }
    public Vector2 GridIndex { get => gridIndex; set => gridIndex = value; }

    private void Awake()
    {
        SetStatusAlive(false);
    }

    public void React(Interactor interactor)
    {
        ToggleStatusAlive();
        OnInteractWithCell.Raise();
    }

    public bool ToggleStatusAlive()
    {
        isAlive = !isAlive;
        OnCellToggleStatus.Invoke(isAlive);
        return isAlive;
    }

    public bool SetStatusAlive(bool value)
    {
        isAlive = value;
        OnCellToggleStatus.Invoke(isAlive);
        return isAlive;
    }

}


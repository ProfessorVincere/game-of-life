using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellFactory : Singleton<CellFactory>
{
    [SerializeField] private GameObject cellPrefab;

    public Cell CreateCell()
    {
       return Instantiate(cellPrefab).GetComponent<Cell>();
    }
}

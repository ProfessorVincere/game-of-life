using UnityEngine;
using UnityAtoms.BaseAtoms;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CellManager : MonoBehaviour
{
    [SerializeField] private CellGrid grid;
    [SerializeField] private SOTransform cellParent;
    [SerializeField] private IntVariable gridSizeRow, gridSizeColumn;
    [SerializeField] private SOTransform middlePoint;
    [SerializeField] private IntVariable totalLivingCells;

    //adjust when callback will happen
    private void Start()
    {
        GenerateGrid();
    }

    public void GenerateGrid()
    {
        DeleteAllForCurrentGrid();
        grid.cellDictionary.Clear();

        float adjustedHorizontalPosition = middlePoint.Value.position.x - (gridSizeRow.Value / 2);
        float adjustedVerticalPosition = middlePoint.Value.position.y - (gridSizeColumn.Value / 2);

        for (int rowIndex = 0; rowIndex < gridSizeRow.Value; rowIndex++)
        {
            for (int columnIndex = 0; columnIndex < gridSizeColumn.Value; columnIndex++)
            {
                //apply settings
                Cell instantiatedCell = CellFactory.Instance.CreateCell();
                instantiatedCell.transform.SetParent(cellParent.Value);
                Vector3 assignedPosition = new Vector3(adjustedHorizontalPosition + (rowIndex * 1), adjustedVerticalPosition + (columnIndex * 1), 0);
                instantiatedCell.transform.position = assignedPosition;
                instantiatedCell.GridIndex = new Vector2(rowIndex, columnIndex);
                instantiatedCell.gameObject.name = "Cell[" + rowIndex + "," + columnIndex + "]";

                //add to data
                grid.cellDictionary.Add(instantiatedCell.GridIndex, instantiatedCell);
            }
        }
    }

    public void DeleteAllForCurrentGrid()
    {
        if (grid.cellDictionary.ToArray().Length > 0)
        {
            foreach (var item in grid.cellDictionary.ToArray())
            {
                if (item.Value != null)
                    Destroy(item.Value.gameObject);
            }
        }
    }

    public void GetTotalNumberOfLivingCells()
    {
        totalLivingCells.Value = 0;

        foreach (var item in grid.cellDictionary.ToArray())
        {
            if (item.Value.IsAlive)
            {
                totalLivingCells.Value++;
            }
        }
    }

    public void RestartLivingCellsCount()
    {
        totalLivingCells.Value = 0;
    }

    private void OnDestroy()
    {
        grid.cellDictionary.Clear();
    }
}

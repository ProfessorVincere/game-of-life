using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName ="Cell System/Data")]
public class CellGrid : SerializedScriptableObject
{
    public Dictionary<Vector2, Cell> cellDictionary = new Dictionary<Vector2, Cell>();
}

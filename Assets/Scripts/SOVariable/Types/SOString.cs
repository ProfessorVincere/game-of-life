﻿
using UnityEngine;

    [CreateAssetMenu(menuName = "Variable/String")]
public class SOString : SOVariable<string>
{
    public override void ResetValue()
    {
        value = "";
    }
}

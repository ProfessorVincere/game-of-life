﻿using UnityEngine;

    [CreateAssetMenu(menuName = "Variable/Layer")]
public class SOLayer : SOVariable<LayerVariable>
{
    public override void ResetValue()
    {
        value.layerValue = default;
        value.layerWeight = default;
        base.ResetValue();
    }

}

[System.Serializable]
public class LayerVariable
{
    public int layerValue;
    public float layerWeight;
}

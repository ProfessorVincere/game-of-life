using UnityEngine;

#pragma warning disable 0649

[CreateAssetMenu(menuName ="Variable/Quaternion")]
public class SOQuaternion : SOVariable<Quaternion>
{
    public override void ResetValue()
    {
        value = Quaternion.identity;
        base.ResetValue();
    }

}

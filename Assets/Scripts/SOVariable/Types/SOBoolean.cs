﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Boolean SO", menuName = "Variable/Boolean")]
public class SOBoolean : SOVariable<bool>
{
    public override void ResetValue()
    {
        value = false;
        base.ResetValue();
    }
}

using UnityEngine;

[CreateAssetMenu(menuName = "Variable/Camera")]
public class SOCamera : SOVariable<Camera> { }

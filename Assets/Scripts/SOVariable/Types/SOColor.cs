﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variable/Color")]
public class SOColor : SOVariable<Color>
{
    public override void ResetValue()
    {
        value = Color.white;
    }
}

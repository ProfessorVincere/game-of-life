using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variable/Player")]
public class SOPlayer : SOVariable<Player> { }

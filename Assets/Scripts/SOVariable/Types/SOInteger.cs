﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variable/Integer")]
public class SOInteger : SOVariable<int> { }

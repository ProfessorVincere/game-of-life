using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SOTransformSetter : SOSetter<SOTransform>
{
    [SerializeField] private Transform transformToSet;

    private void Awake()
    {
        SOVariableToSet.Value = transformToSet;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [CreateAssetMenu(menuName = "Variable/List/Texture")]
public class SOTextureList : SOList<Texture>{ }

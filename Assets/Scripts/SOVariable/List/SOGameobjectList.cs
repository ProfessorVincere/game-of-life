﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [CreateAssetMenu(menuName = "Variable/List/Gameobject")]
public class SOGameobjectList : SOList<GameObject>{ }

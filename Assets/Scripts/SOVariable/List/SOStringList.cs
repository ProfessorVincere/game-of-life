﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variable/List/String")]
public class SOStringList : SOList<string> { }

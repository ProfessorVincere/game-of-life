using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.UI;

public class CellColorConfugurationController : MonoBehaviour
{
    [SerializeField] private ColorVariable aliveColor;
    [SerializeField] private Image colorIndicator;
    [SerializeField] private FlexibleColorPicker colorPicker;

    private void Start()
    {
        colorPicker.StartingColor = aliveColor.Value;
    }

    public void SetColorConfiguation(Color color)
    {
        aliveColor.Value = color;
        colorIndicator.color = color;
        colorPicker.StartingColor = color;
    }
}

using UnityEngine;
using UnityAtoms.BaseAtoms;
using TMPro;

public class GridConfigurationController : MonoBehaviour
{
    [SerializeField] SOGenerationStepManager generationStepManager;
    [SerializeField] IntVariable gridSizeRow, gridSizeColumn;
    [SerializeField] TMP_InputField rowInput, columnInput;

    private void Start()
    {
        rowInput.text = gridSizeRow.Value.ToString();
        columnInput.text = gridSizeRow.Value.ToString();
    }

    public void ApplyGridConfiguration()
    {
        if (string.IsNullOrEmpty(rowInput.text) || string.IsNullOrEmpty(columnInput.text)) return;

        int acquiredRowValue = int.Parse(rowInput.text);
        int acquiredColumnValue = int.Parse(columnInput.text);

        if (acquiredRowValue <= 0)
        {
            acquiredRowValue = 3;
            rowInput.text = acquiredRowValue.ToString();
        }
        if (acquiredColumnValue <= 0)
        {
            acquiredColumnValue = 3;
            columnInput.text = acquiredColumnValue.ToString();
        }
        gridSizeRow.Value = acquiredRowValue;
        gridSizeColumn.Value = acquiredColumnValue;
        generationStepManager.Value.RestartTurnSystem();
    }
}

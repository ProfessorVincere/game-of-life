using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CellSizeConfigurationController : MonoBehaviour
{
    [SerializeField] private FloatVariable currentCellSize;

    public void SetCellSize(float value)
    {
        currentCellSize.Value = value;
    }
}

using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;

public class TimeConfigurationController : MonoBehaviour
{
    [SerializeField] private Slider timeSlider;
    [SerializeField] private FloatConstant minimumSpeed, maximumSpeed;
    [SerializeField] private FloatVariable currentSpeed;

    private void Awake()
    {
        timeSlider.minValue = minimumSpeed.Value;
        timeSlider.maxValue = maximumSpeed.Value;
        timeSlider.value = currentSpeed.Value;
    }

    public void SetTimeSpeed(float value)
    {
        TimeManager.Instance.SetTimeSpeed(value);
    }
}

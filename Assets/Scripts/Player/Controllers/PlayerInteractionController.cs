using UnityEngine.InputSystem;

public class PlayerInteractionController : PlayerController<Interactor>
{
    public override void AwakeMethod()
    {
        base.AwakeMethod();

        playerInputActions.Interaction.Click.performed += (InputAction.CallbackContext context) =>
        {
            controlledObject.ClickAndInteract();
        };
    }
}

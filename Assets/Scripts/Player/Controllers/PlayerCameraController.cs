using UnityEngine.InputSystem;

public class PlayerCameraController : PlayerController<SOCameraManager>
{
    public override void ControlUpdate()
    {
        base.ControlUpdate();

        if (playerInputActions.Move.Horizontal.IsPressed())
        {
            controlledObject.Value.MoveHorizontal(playerInputActions.Move.Horizontal.ReadValue<float>());
        }
        if (playerInputActions.Move.Vertical.IsPressed())
        {
            controlledObject.Value.MoveVertical(playerInputActions.Move.Vertical.ReadValue<float>());
        }
    }
}

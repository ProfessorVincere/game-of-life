using UnityEngine;

[CreateAssetMenu(menuName ="Managers/Generation Step Manager")]
public class SOGenerationStepManager : SOVariable<GenerationStepManager> { }

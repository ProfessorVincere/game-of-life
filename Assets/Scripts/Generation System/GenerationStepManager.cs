using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityAtoms.BaseAtoms;

public class GenerationStepManager : MonoBehaviour
{
    [BoxGroup("References"), SerializeField] private SOGenerationStepManager generationStepManagerInstance;
    [SerializeField] private BoolVariable isTurning;
    [SerializeField] private IntVariable generationCount;
    [BoxGroup("Events"), SerializeField] private VoidEvent OnGenerationStart;
    [BoxGroup("Events"), SerializeField] private VoidEvent OnGenerationEnd;
    [BoxGroup("Events"), SerializeField] private VoidEvent OnGenerationRestart;

    private CountDownTimer turnTimer;

    private bool isGenerationStarted;

    private void Start()
    {
        isTurning.Value = false;
        generationStepManagerInstance.Value = this;
        turnTimer = new CountDownTimer(1f, 0f, 1f);
        SetTurnActive(false);
        RestartTurnSystem();
    }

    public void SetTurnActive(bool value)
    {
        isTurning.Value = value;
    }

    public bool ToggleActive()
    {
        isTurning.Value = !isTurning.Value;
        return isTurning;
    }

    public void RestartTurnSystem()
    {
        isTurning.Value = false;
        isGenerationStarted = false;
        turnTimer.Restart();
        generationCount.Value = 0;
        OnGenerationRestart.Raise();
    }

    private void Update()
    {
        if (!isTurning.Value) return;

        if (!turnTimer.IsFinished())
        {
            if (!isGenerationStarted)
            {
                GenerationStartProcess();
            }

            turnTimer.Tick(Time.deltaTime);
        }
        else
        {
            GenerationEndProcess();
            turnTimer.Restart();
        }
    }

    public void Step()
    {
        isTurning.Value = false;
        turnTimer.Restart();
        StartCoroutine(ExecuteStepSequence());
    }

    IEnumerator ExecuteStepSequence()
    {
        GenerationStartProcess();
        yield return null;
        GenerationEndProcess();
    }

    private void GenerationStartProcess()
    {
        OnGenerationStart.Raise();
        isGenerationStarted = true;
        generationCount.Value++;
    }

    private void GenerationEndProcess()
    {
        isGenerationStarted = false;
        OnGenerationEnd.Raise();
    }
}


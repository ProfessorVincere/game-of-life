using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerationStepController : MonoBehaviour
{
    [SerializeField] private SOGenerationStepManager generationStepManager;

    public void PlayPause()
    {
        generationStepManager.Value.ToggleActive();
    }

    public void Step()
    {
        generationStepManager.Value.Step();
    }

    public void Restart()
    {
        generationStepManager.Value.RestartTurnSystem();
    }
}

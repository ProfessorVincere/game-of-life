using UnityEngine;
using UnityAtoms.BaseAtoms;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private SOCameraManager cameraManager;
    [SerializeField] private SOVirtualCamera mainVirtualCamera;
    [SerializeField] private FloatVariable cameraMoveSpeed;
    [SerializeField] private FloatVariable minimumCellSize, maximumCellSize;
    [SerializeField] private FloatVariable currentCellSize;

    private void Awake()
    {
        cameraManager.Value = this;
    }

    private void Start()
    {
        minimumCellSize.Value = 5;
        currentCellSize.Value = 5;
    }

    //callback from controller
    public void SetZoomValue(float value)
    {
        float adjustedValue = Mathf.Clamp(value, minimumCellSize.Value, maximumCellSize.Value);
        mainVirtualCamera.Value.m_Lens.OrthographicSize = adjustedValue;
    }

    public void MoveHorizontal(float direction)
    {
        mainVirtualCamera.Value.transform.position += direction * cameraMoveSpeed.Value * Vector3.left * Time.deltaTime;
    }

    public void MoveVertical(float direction)
    {
        mainVirtualCamera.Value.transform.position += direction * cameraMoveSpeed.Value * Vector3.down * Time.deltaTime;
    }

    public void ResetCameraPosition()
    {
        mainVirtualCamera.Value.transform.position = new Vector3(0, 0, 10);
    }

    public void CalculateMaxZoom()
    {
        //implement if dynamic based on grid size
    }

    public void SetZoomBasedOnGridSize()
    {
        //implement if dynamic based on grid size
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[CreateAssetMenu(menuName ="Variable/Virtual Camera")]
public class SOVirtualCamera : SOVariable<CinemachineVirtualCamera> { }

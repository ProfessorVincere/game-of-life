using UnityEngine;

[CreateAssetMenu(menuName = "Camera Manager")]
public class SOCameraManager : SOVariable<CameraManager> { }

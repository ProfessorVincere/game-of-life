using UnityEngine;
using Cinemachine;

public class SOVirtualCameraSetter : SOSetter<SOVirtualCamera>
{
    [SerializeField] private CinemachineVirtualCamera targetVirtualCamera;

    private void Awake()
    {
        SOVariableToSet.Value = targetVirtualCamera;
    }
}

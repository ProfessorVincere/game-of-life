using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class DestroyObjectBehavior : MonoBehaviour
{
    public List<GameObject> objectToTurnoff;

    [SerializeField] private UnityEvent OnTurnOffThenCallBack;

    private Action onTurnOffCallback;

    public void DestroyThisObject(float delay)
    {
        Destroy(gameObject, delay);
    }

    public void DestroyThisObject(float delay, Action callback)
    {
        onTurnOffCallback = callback;

        Destroy(gameObject, delay);
    }

    public void TurnOffObject(float delay, Action callback)
    {
        onTurnOffCallback = callback;

        StartCoroutine(TurnOff(delay));
    }

    public void TurnOffThenCallBack(float delay, Action callback)
    {
        onTurnOffCallback = callback;
        objectToTurnoff.ForEach(objectToTurnoff => objectToTurnoff.gameObject.SetActive(false));
        OnTurnOffThenCallBack.Invoke();
        StartCoroutine(ExecuteDelayedCallback(delay));
    }

    IEnumerator TurnOff(float delay)
    {
        yield return new WaitForSeconds(delay);
        objectToTurnoff.ForEach(objectToTurnoff => objectToTurnoff.gameObject.SetActive(false));

    }

    IEnumerator ExecuteDelayedCallback(float delay)
    {
        yield return new WaitForSeconds(delay);
        onTurnOffCallback();
    }
}

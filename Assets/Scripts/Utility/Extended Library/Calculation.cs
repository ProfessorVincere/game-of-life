using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Calculation
{
    public static float NormalizedValue(float currentValue, float minimum, float maximum)
    {
        return (currentValue - minimum) / (maximum - minimum);
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : MonoBehaviour
{
    [SerializeField] private Transform pointToMoveTo;
    [SerializeField] private float movementSpeed;

    // Update is called once per frame
    void Update()
    {
        if (transform.position != pointToMoveTo.position)
        {
            transform.position = Vector3.MoveTowards(transform.position, pointToMoveTo.position, Time.deltaTime * movementSpeed);

            if (Vector3.Distance(transform.position, pointToMoveTo.position) <= 0.25f)
            {
                transform.position = pointToMoveTo.position;
            }
        }
    }
}

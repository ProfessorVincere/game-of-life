using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXHandlerUnpooled : MonoBehaviour
{
    [SerializeField] private GameObject effectPrefab;
    [SerializeField] private float effectDuration;

    public void SpawnEffect(Vector3 position)
    {
        GameObject newEffect = Instantiate(effectPrefab, position, Quaternion.identity);
        Destroy(newEffect, effectDuration);
    }
}

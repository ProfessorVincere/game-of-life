using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSettingsBehaviour : MonoBehaviour
{
	const string INIT_KEY = "Init";

	[SerializeField] AudioSettings bgm;
	[SerializeField] AudioSettings sfx;

	private void OnEnable()
	{
		if(PlayerPrefs.GetInt(INIT_KEY) == 0)
		{
			PlayerPrefs.SetInt(INIT_KEY, 1);
			bgm.UpdateAudio();
			sfx.UpdateAudio();
		}

		bgm.InitializeAudio();
		sfx.InitializeAudio();
	}

	public void ToggleBGM() => bgm.ToggleAudio();

	public void ToggleSFX() => sfx.ToggleAudio();
}

[System.Serializable]
public class AudioSettings
{
	[SerializeField] SOString audioID;
	[SerializeField] UnityEngine.Audio.AudioMixerGroup group;
	[SerializeField] string audioParameter;
	[SerializeField] SOBoolean audioToggle;
	[SerializeField] Sprite audioActiveSprite;
	[SerializeField] Sprite audioInactiveSprite;
	[SerializeField] UnityEngine.UI.Image audioUI;

	public void InitializeAudio()
	{
		audioToggle.Value = HasAudio();
		UpdateAudio();
	}

	public void ToggleAudio()
	{
		audioToggle.Value = !audioToggle.Value;
		
		UpdateAudio();
	}

	public void UpdateAudio()
	{
		PlayerPrefs.SetInt(audioID.Value, audioToggle.Value ? 1 : 0);
		group.audioMixer.SetFloat(audioParameter, Mathf.Log10(audioToggle.Value ? 1 : 0.001f) * 20);
		UpdateVisuals();
	}

	bool HasAudio() => PlayerPrefs.GetInt(audioID.Value) == 1;

	public void UpdateVisuals() => audioUI.sprite = HasAudio() ? audioActiveSprite : audioInactiveSprite;
}
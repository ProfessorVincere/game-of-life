using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMManager : Singleton<BGMManager>
{
	[SerializeField, Range(0, 1)] float smoothness = 0.01f;
	[SerializeField] SOBoolean bgmValue;

	[SerializeField] AudioSource source1;
	[SerializeField] AudioSource source2;

	[SerializeField] float maxVolume = 0.75f;

	bool isSwitched;

	void Awake()
	{
		source1.loop = true;
		source2.loop = true;
	}

	void Update()
	{
		source1.volume = Mathf.Lerp(source1.volume, (isSwitched ? 0 : maxVolume), smoothness);
		source2.volume = maxVolume - source1.volume;
	}

	public void Transit(AudioClip clip)
	{
		isSwitched = !isSwitched;
		PlaySource(isSwitched ? source2 : source1, clip);
	}

	public void SetMaxVolume(float volume) => maxVolume = volume;

	void PlaySource(AudioSource source, AudioClip clip)
	{
		source.clip = clip;
		source.Play();
	}
}
